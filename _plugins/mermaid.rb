require 'jekyll'

module Jekyll
    class MermaidCodeBlock < Jekyll::Generator
        def generate(site)  
            @site = site
            site.posts.docs.each { |post| mermaid post }
        end

        def mermaid(post)
            post.content = post.content.gsub(/```mermaid([\s\S]+?)```/, "{% mermaid %}\\1{% endmermaid %}")
        end
    end
end