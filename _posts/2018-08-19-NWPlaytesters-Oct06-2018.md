---
title: NWPlaytesters at The Sharp Project this October
layout: post
permalink: /blog/nwplaytesters-october-2018/
tags: event
external-url: https://www.eventbrite.co.uk/e/north-west-playtesters-at-the-sharp-project-tickets-49082872192
---

North West Playtesters return to The Sharp Project on Saturday 6th October *(2018)* to present a day of playtesting. We are welcoming board/card, pc, mobile, console and experimental games to be tested.

Free entrance for everyone. We also welcome children aged 13 and above, provided they are supervised with and adult.
- Play and give feedback on the newest games being developed.
- Network with fellow game developers and enthusiasts.
- Join our Troubleshoot Roundtable where we discuss and answer questions about the industry.

[RSVP at Eventbrite](https://www.eventbrite.co.uk/e/north-west-playtesters-at-the-sharp-project-tickets-49082872192).